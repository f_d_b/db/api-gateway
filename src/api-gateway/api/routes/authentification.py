from fastapi import Depends
from fastapi.security import HTTPBearer
from fastapi_jwt_auth import AuthJWT
from fastapi_utils.cbv import cbv
from fastapi_utils.inferring_router import InferringRouter

from redis import Redis

from api.schemas.user import AuthSchema, UserCreateSchema, UserGetSchema
from config.settings import get_settings
from core.exceptions import UnknownUser, PermissionDenied, InvalidCredentials, UniqueFailed
from core.verification import verify_password
from db.crud.user import UserRepository
from db.redis import get_redis_connection


router = InferringRouter()
settings = get_settings()


@cbv(router)
class AuthRoute:
    redis_connection: Redis = Depends(get_redis_connection)

    @router.post('/register', dependencies=[Depends(HTTPBearer())])
    async def register(self, user_data: UserCreateSchema, authorize: AuthJWT = Depends()) -> UserGetSchema:
        """ Создание нового пользователя """
        authorize.jwt_required()

        current_user_id = authorize.get_jwt_subject()
        user = await UserRepository.get_user_by_id(current_user_id)
        if user is None:
            raise UnknownUser
        if not user.is_admin:
            raise PermissionDenied
        await self._check_username_unique(user_data.username)
        await self._check_email_unique(user_data.email)
        return await UserRepository.create_user(user_data)

    @router.post('/login')
    async def login(self, user_credentials: AuthSchema, authorize: AuthJWT = Depends()):
        """ Получение полигона по id """
        user = await UserRepository.get_user_by_email(user_credentials.email)
        if user is None or not verify_password(user_credentials.password, user.password):
            raise InvalidCredentials

        access_token = authorize.create_access_token(subject=user.id)
        refresh_token = authorize.create_refresh_token(subject=user.id)

        return {'access_token': access_token, 'refresh_token': refresh_token}

    @router.post('/refresh', dependencies=[Depends(HTTPBearer())])
    async def refresh(self, authorize: AuthJWT = Depends()):
        authorize.jwt_refresh_token_required()

        current_user_id = authorize.get_jwt_subject()
        new_access_token = authorize.create_access_token(subject=current_user_id)

        return {'access_token': new_access_token}

    @router.post('/logout', dependencies=[Depends(HTTPBearer())])
    async def logout(self, authorize: AuthJWT = Depends()):
        redis_connection: Redis = next(get_redis_connection())

        authorize.jwt_required()

        jti = authorize.get_raw_jwt()['jti']
        self.redis_connection.setex(jti, settings.auth.refresh_expires, 'true')
        self.redis_connection.setex(jti, settings.auth.access_expires, 'true')

        return

    async def _check_username_unique(self, username: str):
        if await UserRepository.get_user_by_username(username) is not None:
            raise UniqueFailed(message='Нарушена уникальность поля username')

    async def _check_email_unique(self, email: str):
        if await UserRepository.get_user_by_email(email) is not None:
            raise UniqueFailed(message='Нарушена уникальность поля email')
