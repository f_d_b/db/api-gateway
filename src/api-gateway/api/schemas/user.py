from pydantic import EmailStr, BaseModel, validator

from core.exceptions import PasswordLengthError


class UserBaseSchema(BaseModel):
    fio: str
    username: str
    email: EmailStr
    is_admin: bool

    class Config:
        orm_mode = True


class UserCreateSchema(UserBaseSchema):
    password: str

    @validator('password')
    def validate_password(cls, value):
        if len(value) < 8:
            raise PasswordLengthError
        return value


class UserGetSchema(UserBaseSchema):
    id: int


class AuthSchema(BaseModel):
    email: EmailStr
    password: str
