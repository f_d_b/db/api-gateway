from tortoise.models import Model
from tortoise import fields


class User(Model):
    __tablename__ = 'user'
    id = fields.BigIntField(pk=True)
    fio = fields.CharField(max_length=150, description='ФИО')
    username = fields.CharField(max_length=150, description='username', unique=True)
    email = fields.CharField(max_length=150, description='email', unique=True)
    password = fields.CharField(max_length=300, description='Пароль')
    is_admin = fields.BooleanField(default=False, description='Является ли администратором')
