from typing import Iterator
from functools import lru_cache
from redis import Redis

from config.settings import get_settings

settings = get_settings()


@lru_cache
def get_redis_connection() -> Iterator[Redis]:
    yield Redis(
        host=settings.redis.HOST,
        port=settings.redis.PORT,
        password=settings.redis.PASSWORD,
        db=0, decode_responses=True,
    )
