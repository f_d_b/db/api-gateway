from api.schemas.user import UserCreateSchema
from db.models.user import User
from core.verification import get_password_hash


class UserRepository:
    @staticmethod
    async def get_user_by_id(id: int):
        return await User.filter(id=id).first()

    @staticmethod
    async def get_user_by_email(email: str):
        return await User.filter(email=email).first()

    @staticmethod
    async def get_user_by_username(username: str):
        return await User.filter(username=username).first()

    @staticmethod
    async def create_user(data: UserCreateSchema):
        hashed_password = get_password_hash(data.password)
        data.password = hashed_password
        user = await User(**data.dict())
        await user.save()

        return user
