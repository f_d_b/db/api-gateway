from fastapi import FastAPI
from tortoise.contrib.fastapi import register_tortoise

from config.settings import get_settings

settings = get_settings()


TORTOISE_ORM = {
    'connections': {
        'default': settings.db.database_url
    },
    'apps': {
        'models': {
            'models': [
                'aerich.models',
                'db.models.user',
            ],
            'default_connection': 'default',
        },
    },
}


def init_db(app: FastAPI) -> None:
    register_tortoise(
        app,
        config=TORTOISE_ORM,
        add_exception_handlers=True,
    )
