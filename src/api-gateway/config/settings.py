from functools import lru_cache
from pydantic import Field, BaseSettings as PydanticBaseSettings


class BaseSettings(PydanticBaseSettings):
    class Config:
        case_sensitive = False


class UvicornSettings(BaseSettings):
    HOST: str = Field('0.0.0.0', env='HOST')
    PORT: int = Field(8000, env='PORT')
    RELOAD: bool = Field(True, env='RELOAD')


class DBSettings(BaseSettings):
    HOST: str = Field('localhost', env='DB_HOST')
    PORT: str = Field('5433', env='DB_PORT')
    NAME: str = Field('gateway', env='DB_NAME')
    USER: str = Field('postgres', env='DB_USER')
    PASSWORD: str = Field('postgres', env='DB_PASSWORD')

    @property
    def database_url(self) -> str:
        return f'postgres://{self.USER}:{self.PASSWORD}@{self.HOST}:{self.PORT}/{self.NAME}'


class RedisSettings(BaseSettings):
    HOST: str = Field('redis', env='REDIS_HOST')
    PORT: str = Field('6379', env='REDIS_PORT')
    USER: str = Field('redis_user', env='REDIS_USER')
    PASSWORD: str = Field('1234', env='REDIS_PASSWORD')

    @property
    def redis_url(self):
        return f'redis://:{self.PASS}@{self.HOST}:{self.PORT}/0'


class AdminSettings(BaseSettings):
    USERNAME: str = Field(default='admin', env='ADMIN_USERNAME')
    FIO: str = Field(default='Иванов Иван Иванович', env='ADMIN_FIO')
    PASSWORD: str = Field(default='321admin123', env='ADMIN_PASSWORD')
    EMAIL: str = Field(default='admin@gmail.com', env='ADMIN_EMAIL')


class AuthSettings(BaseSettings):
    authjwt_secret_key: str = Field(default='auth_secret', env='AUTH_SECRET')
    authjwt_denylist_enabled: bool = True
    authjwt_denylist_token_checks: set = {'access', 'refresh'}
    access_expires: int = Field(default=15 * 60, env='JWT_LIFETIME_SECONDS')
    refresh_expires: int = Field(default=7 * 24 * 60 * 60, env='JWT_LIFETIME_SECONDS')


class Settings(BaseSettings):
    admin: AdminSettings = AdminSettings()
    auth: AuthSettings = AuthSettings()
    db: DBSettings = DBSettings()
    redis: RedisSettings = RedisSettings()
    uvicorn: UvicornSettings = UvicornSettings()

    LOG_FILE_PATH: str = Field('../../logs/logs.log', env='LOG_FILE_PATH')
    LOG_LEVEL: str = Field('debug', env='LOG_LEVEL')


@lru_cache()
def get_settings():
    return Settings()
