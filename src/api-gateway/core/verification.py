from fastapi_jwt_auth import AuthJWT
from passlib.context import CryptContext

from config.settings import get_settings
from db.redis import get_redis_connection

pwd_context = CryptContext(schemes=['bcrypt'], deprecated='auto')
settings = get_settings()


def get_password_hash(password):
    return pwd_context.hash(password)


def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)


@AuthJWT.load_config
def get_config():
    return settings.auth


@AuthJWT.token_in_denylist_loader
def check_if_token_in_denylist(decrypted_token):
    redis_conn = next(get_redis_connection())
    jti = decrypted_token['jti']
    entry = redis_conn.get(jti)
    return entry and entry == 'true'
