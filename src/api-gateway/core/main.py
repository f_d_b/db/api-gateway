from fastapi import FastAPI
from fastapi_jwt_auth.exceptions import AuthJWTException
import uvicorn

from api.routes.authentification import router as auth_router
from core.exceptions import (
    BaseAPIException, exception_handler, authjwt_exception_handler,
    python_exception_handler
)
from config.settings import get_settings
from db.database import init_db


def get_app():
    app = FastAPI()

    @app.on_event('startup')
    async def startup_event():
        init_db(app)

    app.include_router(auth_router, prefix='/auth', tags=['auth'])
    app.exception_handler(AuthJWTException)(authjwt_exception_handler)
    app.exception_handler(BaseAPIException)(exception_handler)
    app.exception_handler(Exception)(python_exception_handler)

    return app


if __name__ == '__main__':
    settings = get_settings()
    uvicorn.run(
        'asgi:app',
        reload=settings.uvicorn.RELOAD,
        host=settings.uvicorn.HOST,
        port=settings.uvicorn.PORT,
        log_level=settings.LOG_LEVEL,
        http='h11',
        loop='asyncio',
    )
