from .base import BaseAPIException


class PasswordLengthError(BaseAPIException):
    """ Слишком короткий пароль """
    status_code = 400
    code = 'password_length_error'
    message = 'Длина пароля должна быть больше 8 символом'


class ExpiredToken(BaseAPIException):
    """ Истек срок действия токена """
    status_code = 400
    code = 'token_has_expired'
    message = 'Истек срок действия токена'


class InvalidToken(BaseAPIException):
    """ Токен недействителен """
    status_code = 400
    code = 'invalid_token'
    message = 'Токена недействителен'


class UnknownUser(BaseAPIException):
    """ Неизвестный пользователь """
    status_code = 400
    code = 'unknown_user'
    message = 'Неизвестный пользователь'


class PermissionDenied(BaseAPIException):
    """ Отказано в доступе """
    status_code = 403
    code = 'permission_denied'
    message = 'Отказано в доступе'


class InvalidCredentials(BaseAPIException):
    """ Неизвестный пользователь """
    status_code = 401
    code = 'invalid_credentials'
    message = 'Неверный логин или пароль'
