FROM python:3.10

COPY requirements.txt .

RUN pip install -r requirements.txt

COPY . /code/
WORKDIR /code/

RUN mkdir logs

ENV PYTHONPATH "/code/src/api-gateway"

RUN chmod a+x scripts/*.sh