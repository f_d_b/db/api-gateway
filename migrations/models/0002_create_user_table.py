from tortoise import BaseDBAsyncClient

from config.settings import get_settings
from core.verification import get_password_hash


async def upgrade(db: BaseDBAsyncClient) -> str:
    settings = get_settings()
    hashed_password = get_password_hash(settings.admin.PASSWORD)

    return f"""
        CREATE TABLE IF NOT EXISTS "user" (
            "id" BIGSERIAL NOT NULL PRIMARY KEY,
            "fio" VARCHAR(150) NOT NULL,
            "username" VARCHAR(150) NOT NULL UNIQUE,
            "email" VARCHAR(150) NOT NULL UNIQUE,
            "password" VARCHAR(300) NOT NULL,
            "is_admin" BOOL NOT NULL  DEFAULT False
        );
        COMMENT ON COLUMN "user"."fio" IS 'ФИО';
        COMMENT ON COLUMN "user"."username" IS 'username';
        COMMENT ON COLUMN "user"."email" IS 'email';
        COMMENT ON COLUMN "user"."password" IS 'Пароль';
        COMMENT ON COLUMN "user"."is_admin" IS 'Является ли администратором';
        INSERT INTO public.user (fio, email, username, password, is_admin)
        VALUES (
            '{settings.admin.FIO}',
            '{settings.admin.EMAIL}',
            '{settings.admin.USERNAME}', 
            '{hashed_password}', 
            True
        );
    """


async def downgrade(db: BaseDBAsyncClient) -> str:
    return """
        DROP TABLE IF EXISTS "user";"""
