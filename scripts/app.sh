#!/bin/bash

aerich upgrade

cd src/api-gateway

gunicorn "asgi:app" --bind 0.0.0.0:8000 --reload --workers 4 --worker-class uvicorn.workers.UvicornWorker